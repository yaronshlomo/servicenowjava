package com.yshlomo.servicenow.injection.components;

import com.yshlomo.servicenow.injection.BScope;
import com.yshlomo.servicenow.injection.modules.RepositoriesModule;

import dagger.Subcomponent;

@BScope
@Subcomponent(modules = {RepositoriesModule.class})
public interface RepositoriesComponent {

    @Subcomponent.Builder
    interface Builder {
        Builder repositoriesModule(RepositoriesModule module);
        RepositoriesComponent build();

    }

}