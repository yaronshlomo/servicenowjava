package com.yshlomo.servicenow.injection.components;

import com.yshlomo.servicenow.injection.AScope;
import com.yshlomo.servicenow.injection.modules.ViewModelModule;
import com.yshlomo.servicenow.ui.DetailsFragment;
import com.yshlomo.servicenow.ui.JokesFragment;
import com.yshlomo.servicenow.ui.MainActivity;

import dagger.Subcomponent;

@AScope
@Subcomponent(modules = {ViewModelModule.class})
public interface ViewsComponents {

    @Subcomponent.Builder
    interface Builder {
        ViewsComponents build();

    }

    void inject(DetailsFragment detailsFragment);
    void inject(MainActivity mainActivity);
    void inject(JokesFragment jokesFragment);
}
