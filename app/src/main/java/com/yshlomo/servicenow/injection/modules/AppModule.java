package com.yshlomo.servicenow.injection.modules;

import android.app.Application;
import android.content.Context;

import com.yshlomo.servicenow.injection.components.RepositoriesComponent;
import com.yshlomo.servicenow.injection.components.ViewsComponents;
import com.yshlomo.servicenow.repositories.ListRepository;
import com.yshlomo.servicenow.viewmodels.JokesViewModel;

import dagger.Module;
import dagger.Provides;

@Module(subcomponents = {ViewsComponents.class, RepositoriesComponent.class})
public class AppModule {

    private Application mApplication;
    private Context mAppContext;
    private JokesViewModel mJokesViewModel;

    public AppModule(Application application){
        mApplication = application;
        mAppContext = application.getBaseContext();
        mJokesViewModel =  new JokesViewModel(mAppContext, new ListRepository(mAppContext));
    }

    @Provides Application provideApplication(){
        return mApplication;
    }

    @Provides Context provideContext(){
        return mAppContext;
    }

}
