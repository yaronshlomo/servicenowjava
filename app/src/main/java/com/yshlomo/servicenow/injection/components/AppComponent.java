package com.yshlomo.servicenow.injection.components;

import com.yshlomo.servicenow.injection.AScope;
import com.yshlomo.servicenow.injection.modules.AppModule;
import com.yshlomo.servicenow.injection.modules.RepositoriesModule;
import com.yshlomo.servicenow.injection.modules.ViewModelModule;

import dagger.Component;

@AScope
@Component(modules = {AppModule.class, ViewModelModule.class, RepositoriesModule.class})
public interface AppComponent{

    ViewsComponents.Builder viesBuilder() ;
    RepositoriesComponent.Builder repositoriesBuilder();

}
