package com.yshlomo.servicenow.injection.modules;

import android.content.Context;

import com.yshlomo.servicenow.repositories.ListRepository;

import dagger.Module;
import dagger.Provides;
import dagger.Reusable;

@Module
public class RepositoriesModule {

    @Provides
    @Reusable
    ListRepository provideListRepository(Context context) {
        return new ListRepository(context);
    }
}
