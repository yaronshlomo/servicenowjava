package com.yshlomo.servicenow.injection.modules;


import android.content.Context;

import com.yshlomo.servicenow.models.DetailsModel;
import com.yshlomo.servicenow.repositories.ListRepository;
import com.yshlomo.servicenow.viewmodels.DetailsViewModel;
import com.yshlomo.servicenow.viewmodels.JokesViewModel;
import com.yshlomo.servicenow.viewmodels.MainScreenViewModel;

import dagger.Module;
import dagger.Provides;
import dagger.Reusable;


@Module
public class ViewModelModule {

    @Provides
    @Reusable
    static MainScreenViewModel providesMainScreenViewModel(Context context) {
        return new MainScreenViewModel(context);
    }

    @Provides
    @Reusable
    static DetailsViewModel providesDetailsViewModel(Context context){
        return new DetailsViewModel(context);
    }

    @Provides
    @Reusable
    static DetailsModel providesDetailsModel(){
        return new DetailsModel();
    }

    @Provides
    @Reusable
     JokesViewModel providesJokesViewModel(Context context){
        return new JokesViewModel(context, new ListRepository(context));
    }

}
