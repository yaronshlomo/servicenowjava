package com.yshlomo.servicenow.retrofit;

import com.yshlomo.servicenow.retrofit.models.ResultPojoModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SearchService {
    @GET("jokes/random?/")
    Observable<ResultPojoModel> getJokes(@Query("firstName") String firstName,
                                         @Query("lastName") String lastName,
                                         @Query("limiTo") String limitTo);
}
