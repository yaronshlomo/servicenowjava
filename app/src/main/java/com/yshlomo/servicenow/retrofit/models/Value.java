package com.yshlomo.servicenow.retrofit.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class Value {
    @SerializedName("id")
    @Expose

    private Integer id;
    @SerializedName("joke")
    @Expose

    private String joke;
    @SerializedName("categories")
    @Expose

    private List categories;


    public final Integer getId() {
        return this.id;
    }

    public final void setId( Integer var1) {
        this.id = var1;
    }


    public final String getJoke() {
        return this.joke;
    }

    public final void setJoke( String var1) {
        this.joke = var1;
    }


    public final List getCategories() {
        return this.categories;
    }

    public final void setCategories( List var1) {
        this.categories = var1;
    }
}
