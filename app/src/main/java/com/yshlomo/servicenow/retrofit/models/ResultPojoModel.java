package com.yshlomo.servicenow.retrofit.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class ResultPojoModel {
    @SerializedName("type")
    @Expose

    private String type;
    @SerializedName("value")

    @Expose
    private Value value;

    public final String getType() {
        return this.type;
    }

    public final void setType( String var1) {
        this.type = var1;
    }

    public final Value getValue() {
        return this.value;
    }

    public final void setValue( Value var1) {
        this.value = var1;
    }
}
