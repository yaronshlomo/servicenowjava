package com.yshlomo.servicenow.repositories;

import android.content.Context;

import com.yshlomo.servicenow.retrofit.SearchService;
import com.yshlomo.servicenow.retrofit.models.ResultPojoModel;

import io.reactivex.Observable;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient.Builder;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.CallAdapter.Factory;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ListRepository {
    private final SearchService mSearchService;

    private final Context mContext;

    public final Observable<ResultPojoModel> getJokes(String firstName, String lastName, String limitTo) {
        return mSearchService.getJokes(firstName, lastName, limitTo);
    }

    public ListRepository( Context context) {
        super();
        mContext = context;
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(Level.BODY);
        Builder httpClient = new Builder();
        httpClient.addInterceptor((Interceptor)logging);
        Retrofit retrofit = (new retrofit2.Retrofit.Builder()).addCallAdapterFactory((Factory)RxJava2CallAdapterFactory.create()).addConverterFactory((retrofit2.Converter.Factory)GsonConverterFactory.create()).client(httpClient.build()).baseUrl("https://api.icndb.com/").build();
        SearchService searchService = retrofit.create(SearchService.class);
        mSearchService = searchService;
    }
}
