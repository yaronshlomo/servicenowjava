package com.yshlomo.servicenow.ui;

import android.app.Application;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jakewharton.rxbinding3.widget.RxTextView;
import com.yshlomo.servicenow.R;
import com.yshlomo.servicenow.R.id;
import com.yshlomo.servicenow.ServiceNowApplication;
import com.yshlomo.servicenow.models.DetailsModel;
import com.yshlomo.servicenow.viewmodels.DetailsViewModel;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public final class DetailsFragment extends Fragment {
    private final String TAG = DetailsFragment.class.getSimpleName();
    private DetailsFragment.DetailsFragmentListener mDetailsFragmentListener;
    private Observable mFirstNameObservable;
    private Observable mLastNameObservable;
    private Disposable mFirstNameDisposable;
    private Disposable mLastNameDisposable;

    @Inject
    DetailsViewModel mDetailsViewModel;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Application serviceNowApplication = getActivity().getApplication();
        View view = inflater.inflate(R.layout.details_fragment, container, false);
        return view;
    }

    public void onViewCreated( View view,  Bundle savedInstanceState) {
        ((ServiceNowApplication)getActivity().getApplication()).mViewsComponents.inject(this);
        super.onViewCreated(view, savedInstanceState);

        mFirstNameObservable = RxTextView.textChanges(getView().findViewById(id.first_name)).map(charSequence -> {
            mDetailsViewModel.onFirstNameChanged(charSequence.toString());
            return true;
        });

        mLastNameObservable = RxTextView.textChanges(getView().findViewById(id.last_name)).map(charSequence -> {
            mDetailsViewModel.onLastNameChanged(charSequence.toString());
            return true;
        });

        getView().findViewById(id.submit_button).setOnClickListener((OnClickListener)(new OnClickListener() {
            public final void onClick(View it) {
                DetailsFragment.DetailsFragmentListener detailsFragmentListener = DetailsFragment.this.mDetailsFragmentListener;
                detailsFragmentListener.onSubmit(mDetailsViewModel.getMFirstName(), mDetailsViewModel.getMLastName());
            }
        }));

        mDetailsViewModel.getJokesLiveData().observe(this, it -> {
            DetailsFragment.this.onDisplayChanged((DetailsModel) it);
        });

        this.getLifecycle().addObserver(mDetailsViewModel);
    }

    public void onResume() {
        super.onResume();
        mFirstNameDisposable = mFirstNameObservable.subscribe();
        mLastNameDisposable = mLastNameObservable.subscribe();
    }

    public void onPause() {
        super.onPause();
        mFirstNameDisposable.dispose();
        mLastNameDisposable.dispose();
    }

    public final void setFragmentListener( DetailsFragment.DetailsFragmentListener detailsFragmentListener) {
        this.mDetailsFragmentListener = detailsFragmentListener;
    }

    private final void onDisplayChanged(DetailsModel detailsModel) {
        Button submitButton = getView().findViewById(R.id.submit_button);
        submitButton.setEnabled(detailsModel.getSubmitEnable());

        EditText firstNameEditText = getView().findViewById(id.first_name);
        EditText lastNameEditText = getView().findViewById(id.last_name);

        ((TextView)(getView().findViewById(R.id.first_name_counter))).setText(detailsModel.getFirstNameConter());
        ((TextView)(getView().findViewById(id.last_name_counter))).setText(detailsModel.getLastNameConter());
        if (detailsModel.getShowError()) {
           mDetailsFragmentListener.onError(true, detailsModel.getErrorMessage());
        }else {
            mDetailsFragmentListener.onError(false, detailsModel.getErrorMessage());
        }

        if(firstNameEditText.getText().length() == 0 && mDetailsViewModel.mFirstName.length() > 0){
            firstNameEditText.setText(mDetailsViewModel.mFirstName);
        }

        if(lastNameEditText.getText().length() == 0 && mDetailsViewModel.mLastName.length() > 0){
            lastNameEditText.setText(mDetailsViewModel.mLastName);
        }


    }

    public interface DetailsFragmentListener {
        void onSubmit(String firstName, String lastName);
        void onError(Boolean isError, String errorMessage);
    }
}
