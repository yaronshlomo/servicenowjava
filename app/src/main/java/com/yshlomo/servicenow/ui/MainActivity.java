package com.yshlomo.servicenow.ui;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import com.yshlomo.servicenow.R;
import com.yshlomo.servicenow.ServiceNowApplication;
import com.yshlomo.servicenow.models.DisplayResultModel;
import com.yshlomo.servicenow.ui.DetailsFragment.DetailsFragmentListener;
import com.yshlomo.servicenow.viewmodels.MainScreenViewModel;
import com.yshlomo.servicenow.viewmodels.MainScreenViewModel.FragmentType;

import javax.inject.Inject;


public final class MainActivity extends AppCompatActivity implements DetailsFragmentListener {

    private final String TAG = MainActivity.class.getSimpleName();

    @Inject
    MainScreenViewModel mMainScreenViewModel;
    Snackbar mSnackbar = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((ServiceNowApplication) getApplication()).mViewsComponents.inject(this);

        mMainScreenViewModel.getSearchResultLiveData().observe(this, displayResultModel -> {
            MainActivity.this.onDisplayChange((DisplayResultModel) displayResultModel);
        });

        getLifecycle().addObserver(mMainScreenViewModel);
    }

    public void onAttachFragment(Fragment fragment) {
        if (fragment instanceof DetailsFragment) {
            DetailsFragment detailsFragment = (DetailsFragment) fragment;
            detailsFragment.setFragmentListener((DetailsFragmentListener) this);
        }
    }

    public void onSubmit(String firstName, String lastName) {
        mMainScreenViewModel.onSubmitData(firstName, lastName);
    }

    @Override
    public void onError(Boolean isError, String errorMessage) {
        if (isError) {
            showError(errorMessage);
        } else if (mSnackbar != null) {
            mSnackbar.dismiss();
        }
    }

    private final void onDisplayChange(DisplayResultModel displayResultModel) {
        if (displayResultModel.replaceFragment == true && displayResultModel.fragmentType == FragmentType.JokesFragment) {
            showJokesFragment(displayResultModel);
            displayResultModel.replaceFragment = false;
        }

        if (displayResultModel.replaceFragment == true && displayResultModel.fragmentType == FragmentType.DetailsFragment) {
            showDetailFragment();
            displayResultModel.replaceFragment = false;
        }

        if(displayResultModel.closeApplication){
            finish();
        }

        if (displayResultModel.getShowError()) {
            onError(true, displayResultModel.getErrorMessage());
            displayResultModel.setShowError(false);
        }else {
            onError(false, "");
        }
    }

    private final void showDetailFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager != null ? fragmentManager.beginTransaction() : null;
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.container);
        frameLayout.setVisibility(View.VISIBLE);
        Fragment detailsFragment = new DetailsFragment();
        Fragment currentFragment = fragmentManager.findFragmentByTag(DetailsFragment.class.getSimpleName());
        if (currentFragment == null /*&& fragmentManager.findFragmentByTag(JokesFragment.class.getSimpleName()) == null*/) {
            fragmentTransaction.replace(R.id.container, detailsFragment, DetailsFragment.class.getSimpleName());
            fragmentTransaction.commit();
        }
    }

    private final void showJokesFragment(DisplayResultModel displayResultModel) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager != null ? fragmentManager.beginTransaction() : null;
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.container);
        frameLayout.setVisibility(View.VISIBLE);

        JokesFragment fragment = new JokesFragment();

        fragment.setArguments(displayResultModel.fragmentArguments);
        Fragment currentFragment = fragmentManager.findFragmentByTag(JokesFragment.class.getSimpleName());
        if (currentFragment == null) {
            fragmentTransaction.replace(R.id.container, fragment, JokesFragment.class.getSimpleName());
            fragmentTransaction.commit();
        }
    }

    private void showError(String message) {
        mSnackbar = Snackbar.make(findViewById(R.id.main_layout), message, 10000);
        mSnackbar.show();

    }

}
