package com.yshlomo.servicenow.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yshlomo.servicenow.R;
import com.yshlomo.servicenow.R.id;
import com.yshlomo.servicenow.ServiceNowApplication;
import com.yshlomo.servicenow.models.JokesListModel;
import com.yshlomo.servicenow.viewmodels.JokesViewModel;

import javax.inject.Inject;


public final class JokesFragment extends Fragment {

    private final String TAG = JokesFragment.class.getSimpleName();
    @Inject
    JokesViewModel mJokesViewModel;


    public static final String FIRST_NAME_KEY = "first_name";

    public static final String LAST_NAME_KEY = "last_name_key";


    public View onCreateView( LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        return inflater.inflate(R.layout.items_fragment, container, false);
    }

    public void onViewCreated( final View view,  Bundle savedInstanceState) {
        ((ServiceNowApplication)getActivity().getApplication()).mViewsComponents.inject(this);
        super.onViewCreated(view, savedInstanceState);
        String firstName = (String) null;
        String lastName = (String) null;

        Bundle arguments = getArguments();
        if (arguments != null) {
            firstName = arguments.getString(FIRST_NAME_KEY);
            lastName = arguments.getString(LAST_NAME_KEY);
        }

        if (firstName != null && firstName.length() != 0 &&
                lastName != null && lastName.length() != 0 && mJokesViewModel.getMSearchAdapter() == null) {
            mJokesViewModel.getJokes(firstName, lastName, "nerdi");
        } else if (mJokesViewModel.getMSearchAdapter() != null) {
            RecyclerView recyclerView =  getView().findViewById(id.jokes_list);
            recyclerView.setAdapter( mJokesViewModel.getMSearchAdapter());
            recyclerView.setLayoutManager((new LinearLayoutManager((Context) getActivity(), 0, false)));
        }

        mJokesViewModel.getJokesLiveData().observe(this, it -> {
            if(((JokesListModel)it).getSetAdapter() && ((RecyclerView)getView().findViewById(id.jokes_list)).getAdapter() == null){
                RecyclerView recyclerView = (RecyclerView) getView().findViewById(id.jokes_list);
                recyclerView.setAdapter((Adapter) ((JokesListModel)it).getJokesAdapter());
                recyclerView.setLayoutManager((LayoutManager) (new LinearLayoutManager((Context) JokesFragment.this.getActivity(), 0, false)));
            }


            if (((JokesListModel)it).getShowError()) {
                Snackbar.make(view, (CharSequence) ((JokesListModel)it).getErrorMessage(), 0).show();
                ((JokesListModel)it).setShowError(false);
            }

            if(((JokesListModel) it).showProgress == true){
                getView().findViewById(R.id.progress).setVisibility(View.VISIBLE);
            }else {
                getView().findViewById(R.id.progress).setVisibility(View.GONE);
            }
        });
        getLifecycle().addObserver( mJokesViewModel);
    }
}
