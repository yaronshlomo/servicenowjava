package com.yshlomo.servicenow.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yshlomo.servicenow.R;
import com.yshlomo.servicenow.retrofit.models.ResultPojoModel;
import com.yshlomo.servicenow.retrofit.models.Value;

import java.util.List;


public class JokesAdapter extends Adapter {
    private final String TAG;
    private final List items;
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final JokesAdapter.PositionListener mPositionListener;

    public JokesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = mInflater.inflate(R.layout.list_item_layout, parent, false);
        return new JokesAdapter.ViewHolder((CardView) view);
    }


    public int getItemCount() {
        return items.size();
    }

    public void onBindViewHolder(JokesAdapter.ViewHolder holder, int position) {
        mPositionListener.onPositionChanged(position);
        Value value = ((ResultPojoModel) items.get(position)).getValue();
        holder.textView.setText((CharSequence) (value != null ? value.getJoke() : null));
    }

    // $FF: synthetic method
    // $FF: bridge method
    public void onBindViewHolder(android.support.v7.widget.RecyclerView.ViewHolder viewHolder, int position) {
        onBindViewHolder((JokesAdapter.ViewHolder) viewHolder, position);
    }

    public JokesAdapter(Context context, List data, JokesAdapter.PositionListener positionListener) {
        mPositionListener = positionListener;
        TAG = JokesAdapter.class.getSimpleName();
        items = data;
        mContext = context;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        mInflater = inflater;
    }

    public final class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        private TextView textView;

        public final TextView getTextView() {
            return textView;
        }

        public ViewHolder(CardView itemView) {
            super(itemView);
            View item = itemView.findViewById(R.id.item_view);
            textView = (TextView) item;
        }
    }

    public interface PositionListener {
        void onPositionChanged(int var1);
    }
}
