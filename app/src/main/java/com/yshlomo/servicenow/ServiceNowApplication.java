package com.yshlomo.servicenow;

import android.app.Application;

import com.yshlomo.servicenow.injection.components.AppComponent;
import com.yshlomo.servicenow.injection.components.DaggerAppComponent;
import com.yshlomo.servicenow.injection.components.ViewsComponents;
import com.yshlomo.servicenow.injection.modules.AppModule;
import com.yshlomo.servicenow.injection.modules.RepositoriesModule;
import com.yshlomo.servicenow.injection.modules.ViewModelModule;

import java.util.ArrayList;
import java.util.List;



public final class ServiceNowApplication extends Application {

    public AppComponent mComponent;

    public ViewsComponents mViewsComponents;

    public AppComponent getComponent() {
        return mComponent;
    }

    public List mJokesListItems = new ArrayList();

    @Override
    public void onCreate() {
        super.onCreate();

        mComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .repositoriesModule(new RepositoriesModule())
                .viewModelModule(new ViewModelModule())
                .build();

        mComponent.repositoriesBuilder().build();

        mViewsComponents = mComponent.viesBuilder().build();
    }
}
