package com.yshlomo.servicenow.viewmodels;

import android.arch.lifecycle.Lifecycle.Event;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ViewModel;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;

import com.yshlomo.servicenow.R;
import com.yshlomo.servicenow.models.DisplayResultModel;


public final class MainScreenViewModel extends ViewModel implements LifecycleObserver {
    private final String TAG;
    private final Context mContext;

    private final MediatorLiveData<DisplayResultModel> mSearchResultLiveData;
    private DisplayResultModel mDisplayResultModel;

    private BroadcastReceiver NetworkChangeListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!isDataConnected()) {
                mDisplayResultModel.setShowError(true);
                mDisplayResultModel.setErrorMessage(mContext.getString(R.string.no_network));
            }else{
                mDisplayResultModel.setShowError(false);
            }
            mSearchResultLiveData.postValue(mDisplayResultModel);
        }
    };

    public final MediatorLiveData getSearchResultLiveData() {
        return mSearchResultLiveData;
    }

    public final void onSubmitData( String firstName,  String lastName) {
        Bundle bundle = new Bundle();
        bundle.putString("first_name", firstName);
        bundle.putString("last_name_key", lastName);
        mDisplayResultModel.replaceFragment = true;
        mDisplayResultModel.fragmentArguments = bundle;
        mDisplayResultModel.fragmentType = MainScreenViewModel.FragmentType.JokesFragment;
        mSearchResultLiveData.postValue(mDisplayResultModel);
    }

    @OnLifecycleEvent(Event.ON_START)
    public void onStart() {
        Log.d(TAG, "newInstance onStart: " + this);

        IntentFilter networkChangeIntentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        mContext.registerReceiver(NetworkChangeListener, networkChangeIntentFilter);
        mDisplayResultModel.replaceFragment = true;
        mSearchResultLiveData.postValue(mDisplayResultModel);

    }

    @OnLifecycleEvent(Event.ON_STOP)
    public void onStop(){
        mContext.unregisterReceiver(NetworkChangeListener);
    }

    public MainScreenViewModel(Context context) {
        super();
        TAG = MainScreenViewModel.class.getSimpleName();
        mContext = context;
        mSearchResultLiveData = new MediatorLiveData();
        mDisplayResultModel = new DisplayResultModel();
    }

    public static enum FragmentType {
        DetailsFragment,
        JokesFragment;
    }

    public void onBackPressed(){
        if(mDisplayResultModel.fragmentType == FragmentType.JokesFragment){
            mDisplayResultModel.replaceFragment = true;
            mDisplayResultModel.fragmentType = FragmentType.DetailsFragment;
            mSearchResultLiveData.postValue(mDisplayResultModel);
        }else {
            mDisplayResultModel.closeApplication = true;
            mSearchResultLiveData.postValue(mDisplayResultModel);
        }
    }

    private boolean isDataConnected() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            return connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
        } catch (Exception e) {
            return false;
        }
    }
}
