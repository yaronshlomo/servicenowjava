package com.yshlomo.servicenow.viewmodels;

import android.arch.lifecycle.Lifecycle.Event;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.util.Log;

import com.yshlomo.servicenow.adapters.JokesAdapter;
import com.yshlomo.servicenow.adapters.JokesAdapter.PositionListener;
import com.yshlomo.servicenow.models.JokesListModel;
import com.yshlomo.servicenow.repositories.ListRepository;
import com.yshlomo.servicenow.retrofit.models.ResultPojoModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class JokesViewModel extends ViewModel implements LifecycleObserver, PositionListener {

    private final MediatorLiveData<JokesListModel> mJokesLiveData;

    private JokesAdapter mSearchAdapter;
    private List mListItems;
    private String mLastName;
    private String mFirstName;
    private String mLimitTo;
    private String TAG;
    private JokesListModel mDisplayResultModel = new JokesListModel();

    private final Context mContext;

    public ListRepository mRepository;

    public final MediatorLiveData getJokesLiveData() {
        return this.mJokesLiveData;
    }

    public final JokesAdapter getMSearchAdapter() {
        return this.mSearchAdapter;
    }

    public final void onResult( ResultPojoModel searchResult) {
        this.mDisplayResultModel = new JokesListModel();
        this.mListItems.add(searchResult);
        if (this.mSearchAdapter == null) {
            this.mSearchAdapter = new JokesAdapter(this.mContext, this.mListItems, (PositionListener) this);
            JokesListModel jokesListModel = this.mDisplayResultModel;

            jokesListModel.setJokesAdapter(this.mSearchAdapter);

            mDisplayResultModel.setSetAdapter(true);
        } else {
            if (this.mSearchAdapter != null) {
                mSearchAdapter.notifyItemInserted(this.mListItems.size());
            }
        }
        mDisplayResultModel.setShowError(false);
        mDisplayResultModel.setErrorMessage("");
        mDisplayResultModel.showProgress = false;
        mJokesLiveData.postValue(mDisplayResultModel);
    }

    @OnLifecycleEvent(Event.ON_CREATE)
    public final void onCreate() {
        Log.d(TAG, "onStart: " + this);
    }

    public final void getJokes( String firstName,  String lastName,  String limitTo) {
        this.mLastName = lastName;
        this.mFirstName = firstName;
        this.mLimitTo = limitTo;

        this.mRepository.getJokes(firstName, lastName, limitTo).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(it -> {onResult((ResultPojoModel) it);}, throwable -> {Log.e(TAG, throwable.toString());});
        if(mDisplayResultModel == null){
            mDisplayResultModel = new JokesListModel();
        }
        mDisplayResultModel.showProgress = true;
        mJokesLiveData.postValue(mDisplayResultModel);

    }

    public void onPositionChanged(int position) {
        if (this.mListItems.size() == position + 1) {
            String firstName = this.mFirstName;
            String lastName = this.mLastName;
            String limitTo = this.mLimitTo;

            this.getJokes(firstName, lastName, limitTo);
        }
    }


    public JokesViewModel(Context mContext, ListRepository listRepository) {
        super();
        this.mContext = mContext;
        this.mRepository = listRepository;
        this.mJokesLiveData = new MediatorLiveData();
        this.mListItems = new ArrayList<JokesListModel>();
        this.TAG = JokesViewModel.class.getSimpleName();
    }

    private void onError(String message){
        mDisplayResultModel.setShowError(true);
        mDisplayResultModel.setErrorMessage(message);
        mJokesLiveData.postValue(mDisplayResultModel);
    }
}
