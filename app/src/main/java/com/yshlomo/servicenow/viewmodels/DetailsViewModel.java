package com.yshlomo.servicenow.viewmodels;

import android.arch.lifecycle.Lifecycle.Event;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.util.Log;

import com.yshlomo.servicenow.R;
import com.yshlomo.servicenow.models.DetailsModel;


public final class DetailsViewModel extends ViewModel implements LifecycleObserver {

    private final String TAG = DetailsViewModel.class.getSimpleName();
    public String mFirstName;

    public String mLastName;

    private DetailsModel mDetailsModel;

    public String mCounterFormat;

    private final MediatorLiveData mJokesLiveData;

    private final Context mContext;

    public DetailsViewModel( Context context) {
        super();
        Log.d(TAG, "newInstance DetailsViewModel: " + this);
        mContext = context;
        mDetailsModel = new DetailsModel();
        mJokesLiveData = new MediatorLiveData();
    }


    public final String getMFirstName() {
        return mFirstName;
    }

    public final void setMFirstName( String firstName) {
        mFirstName = firstName;
    }


    public final String getMLastName() {
        return mLastName;
    }

    public final void setMLastName( String lastName) {
        mLastName = lastName;
    }


    public final DetailsModel getMDetailsModel() {
        return mDetailsModel;
    }

    public final void setMDetailsModel( DetailsModel detailsModel) {
        mDetailsModel = detailsModel;
    }


    public final String getMCounterFormat() {
        return mCounterFormat;
    }

    public final void setMCounterFormat( String var1) {
        mCounterFormat = var1;
    }


    public final MediatorLiveData getJokesLiveData() {
        return mJokesLiveData;
    }

    @OnLifecycleEvent(Event.ON_CREATE)
    public final void onCreate() {
        enableSubmitButton();
        mCounterFormat = mContext.getString(R.string.counter_format);
    }

    public final void onFirstNameChanged( String firstName) {
        mFirstName = firstName;
        mDetailsModel.setFirstNameLength(firstName.length());
        mDetailsModel.setShowError(false);
        int firstNameLength = mDetailsModel.getFirstNameLength();

        String counterFormat = String.format(mCounterFormat, firstNameLength);
        mDetailsModel.setFirstNameConter(counterFormat);
        if (mDetailsModel.getFirstNameLength() >= 20) {
            mDetailsModel.setShowError(true);
            mDetailsModel.setErrorMessage(mContext.getString(R.string.max_length_error));
        }

        mDetailsModel.firstName = firstName;

        enableSubmitButton();
        mJokesLiveData.postValue(mDetailsModel);
    }

    public final void onLastNameChanged( String lastName) {
        mLastName = lastName;
        mDetailsModel.setLastNameLength(lastName.length());

        mDetailsModel.setShowError(false);

        int lastNameLength = mDetailsModel.getLastNameLength();

        mDetailsModel.setLastNameConter(String.format(mCounterFormat, lastNameLength));
        if (mDetailsModel.getLastNameLength() >= 20) {
            mDetailsModel.setShowError(true);
            mDetailsModel.setErrorMessage(mContext.getString(R.string.max_length_error));
        }

        mDetailsModel.lastName = lastName;

        enableSubmitButton();
        mJokesLiveData.postValue(mDetailsModel);
    }

    private final void enableSubmitButton() {
        mDetailsModel.setSubmitEnable(mDetailsModel.getFirstNameLength() > 0 && mDetailsModel.getLastNameLength() > 0);
    }



}
