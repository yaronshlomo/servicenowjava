package com.yshlomo.servicenow.models;


public final class DetailsModel extends BaseModel {
    private int firstNameLength;
    private int lastNameLength;
    private String firstNameConter;
    private String lastNameConter;
    private boolean submitEnable;

    public String lastName = "";
    public String firstName = "";

    public final int getFirstNameLength() {
        return this.firstNameLength;
    }

    public final void setFirstNameLength(int var1) {
        this.firstNameLength = var1;
    }

    public final int getLastNameLength() {
        return this.lastNameLength;
    }

    public final void setLastNameLength(int var1) {
        this.lastNameLength = var1;
    }

    public final String getFirstNameConter() {
        return this.firstNameConter;
    }

    public final void setFirstNameConter( String var1) {
        this.firstNameConter = var1;
    }

    public final String getLastNameConter() {
        return this.lastNameConter;
    }

    public final void setLastNameConter( String var1) {
        this.lastNameConter = var1;
    }

    public final boolean getSubmitEnable() {
        return this.submitEnable;
    }

    public final void setSubmitEnable(boolean var1) {
        this.submitEnable = var1;
    }

    public DetailsModel(){
        this.firstNameLength = 0;
        this.lastNameLength = 0;
        this.firstNameConter = "";
        this.lastNameConter = "";
        this.submitEnable = false;
    }

    public DetailsModel(int firstNameLength, int lastNameLength,  String firstNameConter,  String lastNameConter, boolean submitEnable) {
        this.firstNameLength = firstNameLength;
        this.lastNameLength = lastNameLength;
        this.firstNameConter = firstNameConter;
        this.lastNameConter = lastNameConter;
        this.submitEnable = submitEnable;
    }
}
