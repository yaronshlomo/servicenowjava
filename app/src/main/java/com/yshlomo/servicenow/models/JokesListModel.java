package com.yshlomo.servicenow.models;

import com.yshlomo.servicenow.adapters.JokesAdapter;

public final class JokesListModel extends BaseModel {

    private JokesAdapter jokesAdapter;
    private boolean setAdapter;


    public final JokesAdapter getJokesAdapter() {
        return this.jokesAdapter;
    }

    public final void setJokesAdapter( JokesAdapter jokesAdapter) {
        this.jokesAdapter = jokesAdapter;
    }

    public final boolean getSetAdapter() {
        return this.setAdapter;
    }

    public final void setSetAdapter(boolean adapter) {
        this.setAdapter = adapter;
    }

    public Boolean showProgress = false;

    public JokesListModel(){
        jokesAdapter = null;
        setAdapter = false;
    }
}
