package com.yshlomo.servicenow.models;


public class BaseModel {
    private boolean showError;
    private String errorMessage;

    public final boolean getShowError() {
        return this.showError;
    }

    public final void setShowError(boolean showError) {
        this.showError = showError;
    }

    public final String getErrorMessage() {
        return this.errorMessage;
    }

    public final void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public BaseModel(boolean showError, String errorMessage) {
        this.showError = showError;
        this.errorMessage = errorMessage;
    }

    public BaseModel() {
        this.showError = false;
        this.errorMessage = "";
    }
}
