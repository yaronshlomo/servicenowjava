package com.yshlomo.servicenow.models;

import android.os.Bundle;

import com.yshlomo.servicenow.viewmodels.MainScreenViewModel.FragmentType;

public final class DisplayResultModel extends BaseModel {
    public boolean closeKeyboard;

    public FragmentType fragmentType;

    public Bundle fragmentArguments;

    public Boolean replaceFragment;

    public Boolean closeApplication = false;

    public DisplayResultModel() {
        super();
        closeKeyboard = false;
        fragmentType = FragmentType.DetailsFragment;
        fragmentArguments = new Bundle();
        replaceFragment = true;
        
    }
}
